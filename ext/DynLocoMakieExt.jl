module DynLocoMakieExt # functions using Makie for plotting

using CairoMakie
using DynLoco
using StaticArrays: SVector
import DynLoco: multistepsensitivity, plot2dsensitivity, plot3dsensitivity, boundingrect, plot2dsensitivity!, plot3dsensitivity!

"""
    `plot2dsensitivity(steps, Ps, workcosts, timecosts; Pmultiplier, numcols = 4, showtotal = false)`
produces 2D plots of the sensitivity of the work and time costs to
variations in push-offs `Ps`. The data `steps`, `Ps`, `workcosts`, and
`timecosts` should all be m x n arrays, where m is # of variations and
n is number of steps. The 2D plots are in an array, each with axes `Ps` and costs.
The `workcosts` and `timecosts` should be of same scale, and are plotted with offsets
to coincide at the optimum at 0. Use `showtotal = true` to plot the work + time costs
as a third line.

Outputs: `f`, `ax` are the Makie figure and axis objects.

Optional argument `Pmultiplier = 0.9:0.02:1.1`
sets the variation factors for push-offs. Optional `numcols = 4` sets the number of columns in the plot.
"""
function plot2dsensitivity(steps, Ps, workcosts, timecosts, Pmultiplier; numcols = 4, otherargs...)
    f = Figure()
    axs = [Axis(f[(i-1) ÷ numcols + 1, (i-1) % numcols + 1]) for i in axes(workcosts,2)]
    plot2dsensitivity!(axs, steps, Ps, workcosts, timecosts, Pmultiplier; numcols = numcols, otherargs...)    
    return f, axs
end

function plot2dsensitivity!(axs::Vector{Axis}, steps, Ps, workcosts, timecosts, Pmultiplier; numcols = 4, scalebarx = [-0.045, -0.045, -0.025],
    scalebary = [0.005, 0, 0], boxrect = nothing, showtotal = false) # x scale is .02, y scale is 0.005 
    if boxrect === nothing; boxrect = boundingrect(Ps, workcosts, timecosts, dims=2); end
    # boxrect puts the plots in 2d boxes to help with scale & location
    numsteps = size(workcosts, 2) # number of steps provided
    whichbest = (length(Pmultiplier)+1) ÷ 2 # index of optimal P

    for (i,ax) in enumerate(axs) # go through each of the steps (typically 1:7)
        lines!(ax, Ps[:,i], workcosts[:,i], color=:blue, linewidth=2, label="work")
        lines!(ax, Ps[:,i], timecosts[:,i], color=:red, linewidth=2, label="time")
        if showtotal
            lines!(ax, Ps[:,i], workcosts[:,i] .+ timecosts[:,i], color=:green, linewidth=2, label="total")
        end
        scatter!(ax, Ps[whichbest,i], workcosts[whichbest,i], color=:black, markersize=8) # a dot at the optimum
        wireframe!(ax, boxrect + Vec(Ps[whichbest,i],0),color=:gray,linewidth=0.5) # bounding rectangle
        lines!(ax, Ps[whichbest,i].+ scalebarx,workcosts[whichbest,i] .+ scalebary,color=:gray,linewidth=2) # a couple consistent scale bars
    end
    axs[1].xlabel="Push-off P_i"
    axs[1].ylabel="Energy"
    linkaxes!(axs...)
end

plot2dsensitivity(steps, Ps, workcosts, timecosts; Pmultiplier=0.9:0.02:1.1, numcols=4, otherargs...) = plot2dsensitivity(steps, Ps, workcosts, timecosts, Pmultiplier; numcols=numcols, otherargs...)

"""
    `plot2dsensitivity(w::Walk, optresults::MultiStepResults)`
produces 2D sensitivity plots for walk object `w` and optimal steps `optresults`. 
This automatically calls `multistepsensitivity` to perform the sensitivity analysis. 
Optional arguments: Pmultiplier, numcols, centering, ct
"""
plot2dsensitivity(w::Walk, optresults::MultiStepResults; Pmultiplier = 0.9:0.02:1.1, numcols=4, centering = true, ct = optresults.metadata.ct, showtotal=false, otherargs...) = 
    plot2dsensitivity(multistepsensitivity(w, optresults; Pmultiplier = Pmultiplier, centering = centering, ct = ct)...; numcols=numcols, showtotal=showtotal, otherargs...)

plot2dsensitivity!(axs::Vector{Axis}, w::Walk, optresults::MultiStepResults; scalebarx = [-0.045, -0.045, -0.025],
    scalebary = [0.005, 0, 0], otherargs...) = plot2dsensitivity!(axs, multistepsensitivity(w, optresults)...; scalebarx=scalebarx, scalebary=scalebary, boxrect=boxrect, otherargs...)

"""
    `plot3dsensitivity(steps, Ps, workcosts, timecosts; Pmultiplier)`
produces a 3D plot of the sensitivity of the work and time costs to 
variations in push-offs `Ps`. The data `steps`, `Ps`, `workcosts`, and
`timecosts` should all be m x n arrays, where m is # of variations and
n is number of steps. The 3D plot has axes `steps`, `Ps`, and costs.
The `workcosts` and `timecosts` should all be of same scale.
Optional argument `Pmultiplier = 0.9:0.02:1.1`
sets the variation factors for push-offs.

Outputs: `f`, `ax` are the Makie figure and axis objects.

    `plot3dsensitivity(w::Walk, stepresults::MultistepResults)`
produces a 3D sensitivity plot for walk object `w` and optimal steps
`stepresults`. This automatically calls `multistepsensitivity` to
perform the sensitivity analysis. 
"""
function plot3dsensitivity(steps, Ps, workcosts, timecosts, Pmultiplier)
    f = Figure(;resolution=(600,900)) # from default 800x600
    ax = Axis3(f[1,1], azimuth=-pi/2.8, aspect=(2,1,1), xlabel="Step i", ylabel="Push-off P_i", zlabel="Energy")
    f,ax = plot3dsensitivity!(ax, steps, Ps, workcosts, timecosts, Pmultiplier)
    return f, ax
end

plot3dsensitivity(steps, Ps, workcosts, timecosts; Pmultiplier=0.9:0.02:1.1) = plot3dsensitivity(steps, Ps, workcosts, timecosts, Pmultiplier)

plot3dsensitivity(w::Walk, optresults::MultiStepResults; Pmultiplier = 0.9:0.02:1.1, centering = true, ct = optresults.metadata.ct) = plot3dsensitivity(multistepsensitivity(w, optresults; Pmultiplier = Pmultiplier, centering = centering, ct = ct)...)

plot3dsensitivity!(ax::Axis3, w::Walk, optresults::MultiStepResults; Pmultiplier = 0.9:0.02:1.1, centering = true, ct = optresults.metadata.ct) = plot3dsensitivity!(ax,multistepsensitivity(w, optresults; Pmultiplier=Pmultiplier, centering = centering, ct = ct)...)

function plot3dsensitivity!(ax::Axis3, steps, Ps, workcosts, timecosts, Pmultiplier)
    whichbest = (length(Pmultiplier)+1) ÷ 2 # index of optimal P
    boxrect = boundingrect(Ps, workcosts, timecosts) # put the plots in 2d boxes to help with scale & location
    f = ax.parent
    ax.azimuth = -pi/2.8; ax.aspect=(2,1,1); ax.xlabel="Step i"; ax.ylabel="Push-off P_i"; ax.zlabel="Energy"
    scalebarx = [-0.045, -0.045, -0.025] # x scale of length 0.02
    scalebary = [0.005, 0, 0] # y scale is .005 
    for i in axes(workcosts,2) # go through each of the steps (typically 1:7)
        lines!(ax, steps[:,i], Ps[:,i], workcosts[:,i], color=:blue, linewidth=2, label="work")
        lines!(ax, steps[:,i], Ps[:,i], timecosts[:,i], color=:red, linewidth=2, label="time")
        scatter!(ax, steps[whichbest,i], Ps[whichbest,i], workcosts[whichbest,i], color=:black, markersize=8) # a dot at the optimum
        wireframe!(ax, boxrect + Vec(i-1,Ps[whichbest,i],0),color=:gray,linewidth=0.5) # bounding rectangle
        lines!(ax, (i-1)*[1,1,1], Ps[whichbest,i].+ scalebarx,workcosts[whichbest,i] .+ scalebary,color=:gray,linewidth=2) # a couple consistent scale bars
    end
    return f, ax
end




# boundingrectangle is internal utility, not exported
"""
    `boundingrect(Ps, workcosts, timecosts; dims=3)`
produces a minimal bounding box for the range of `Ps` on horizontal axis,
and the range of `workcosts` and `timecosts` on the vertical axis (assuming
they are on equal scale). The rectangle is a `GeometryBasics.jl` object of
dimensionality `dims` (2 or 3).
"""
function boundingrect(Ps, workcosts, timecosts; dims=3)
    dx = 0.5*(maximum(Ps) - minimum(Ps)) # half the full x range (enclosing all Ps)
    dy = max(maximum(workcosts), -minimum(workcosts), 
             maximum(timecosts), -minimum(timecosts)) # half the y range
    if dims == 2
        Rect2(Vec2(-dx,-dy),Vec2(2dx,2dy))    
    else
        Rect3(Vec3(0, -dx, -dy),Vec3(0, 2dx, 2dy)) # rect format is corner, sides
    end
end


end # module