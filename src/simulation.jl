# continuous-time simulations
# so far only for WalkRW2l

# simulatestep for a single step, and simulatesteps for a multistep walk

export simulatestep

"""
    `simulatestep(walk [,parameters])`

Simulate one step with a `walk` struct, from mid-stance to mid-stance,
and return the continuous-time time t plus states θ and Ω. Continuous-
time states are useful for plotting. Here they include a doubled time
point for the step-to-step transition, for the states immediately before and
after. The simulation uses linearized inverted pendulum dynamics for
the stance leg angle, and energy conservation for its angular velocity.

Optional keyword args: n = 12 # of time steps to simulate (including endpoints),
height = 0 beginning step height, POCO=false should PO & CO work be included in
KE.

Parameter values can be supplied to override the existing field values. 
Notably, includes vm, P, δangle.
Output is arrays (t, θ=theta, Ω=thetadot, PE, KE, nextheight) in a named tuple.
See `onestep` to get just the discrete mid-stance states.
"""
simulatestep(w::WalkRW2l; n = 12, height = 0, POCO=false, options...) =
    _simulatestep(w, StepResults(onestep(w; options...)...); n=n, height=height, POCO=POCO)

"""
    `simulatestep(walk, stepresult::StepResults)` uses `onestep` results
"""
simulatestep(w::WalkRW2l, stepresult::StepResults; n = 12, height = 0, POCO=false) = 
    _simulatestep(w, stepresult; n=n, height=height, POCO=POCO)

function _simulatestep(w::WalkRW2l, stepresult::StepResults; n = 12, height = 0, POCO=false, options...)
    # find end conditions of a step, destructure into local variables
    (; vm, vm0,θnew,tf,tf1,tf2,Ωplus,steplength, δ, Pwork, Cwork, P) = stepresult
    (; g, L, γ) = w;
    n1 = Int(floor((n+1)/2)) # allocate time steps before & after s2s transition
    n2 = n - n1            
    t1 = LinRange(0, tf1, n1)  # time goes from 0 to tf1 (just before s2s transition)
    t2 = LinRange(0, tf2, n2) #  then from 0 (just after s2s transition) to tf2
    # where we treat each phase as starting at t=0. For plotting we will assemble
    # a single time vector for both phases
    ωₙ = g / L  
 
    etw = exp.(ωₙ*t1); emtw = 1 ./ etw # char roots of linearized inverted pendulum are ±ωₙ
    # Leg angle vs. time, first part of stance
    θ1 = @. -(emtw*(etw-1)*((etw+1)*vm0)+(etw-1)*L*γ*ωₙ)/(2*L*ωₙ)  # could be simplified but works as is 
 
    # second part of stance
    etw = exp.(ωₙ*t2); emtw = 1 ./ etw # char roots of linearized inverted pendulum are ±ωₙ
    θ2 = @. emtw *((etw*etw-1)*Ωplus + (-(etw-1)^2*γ + (etw*etw+1)*θnew)*ωₙ)/(2*ωₙ) 
 
    nextheight = height + steplength*sin(δ) # height of terrain
    PE1 = cos.(θ1) .- 1 .+ height
    PE2 = cos.(θ2) .+ nextheight .- 1
    KE1 = 1/2*vm0^2 .- cos.(θ1) .+ 1
    KE2 = 1/2*vm^2 .- cos.(θ2) .+ 1
 
    # Use energy conservation to find velocities, to be consistent with onestep
    Ω1 = -sqrt.(2*KE1)
    Ω2 = -sqrt.(2*KE2)
    # we're not using the exponential form of velocities, but here's what they look like:
    #Ω1 = @. -(emtw*(etw*etw+1)*vm0+(etw*etw-1)*L*γ*ωₙ)/(2*L)
    #Ω2 = @. 0.5*emtw*((etw*etw+1)*Ωplus - (etw*etw-1)*(γ-θnew)*ωₙ)
    # Ω = [Ω1; Ω2] 
 
    v1 = hcat(-Ω1.*cos.(θ1), -Ω1.*sin.(θ1)) # horizontal & vertical velocities
    v2 = hcat(-Ω2.*cos.(θ2), -Ω2.*sin.(θ2)) # for each phase

    if !POCO # just splice the two phases together if not including s2s
        t = [t1; t2 .+ tf1]
        θ=[θ1; θ2]
        KE = [KE1; KE2]
        PE = [PE1; PE2]
        Ω = [Ω1; Ω2]
        v = [v1;v2] # horizontal & vertical velocities, as a Nx2 matrix

    else # include push-off and collision work of s2s
        t = [t1; t1[end]; t2 .+ tf1] # s2s occurs at end of t1
        θ=[θ1; θ1[end]; θ2] # no change in leg angle during s2s
        KE = [KE1; KE1[end] + Pwork; KE2] # accumulate push-off for mid-s2s
        PE = [PE1; PE1[end]; PE2] # no change in PE
        Ω = [-sqrt.(2*KE1); NaN; -sqrt.(2*KE2)] # blank the angular velocities during s2s
        vmid = [v1[end,1]-P*sin(θ1[end]) v1[end,2]+P*cos(θ1[end])]
        v = [v1;vmid;v2] # horizontal & vertical velocities
    end
 
    return (t=t, θ=θ, Ω=Ω, PE=PE, KE=KE, v=v, nextheight=nextheight)
end

export simulatesteps

"""
    `simulatesteps(walk::Walk, msr::MultiStepResults)`
Simulate multiple steps with a `walk` struct, and return a named tuple with
fields `t`, `θ`, `Ω` (angular velocity), `KE`, `PE`.

Optional keyword args: `n=12` # of time steps per walking step, `POCO=true` whether
to include push-off & collision work in `KE`, `tchange=0` time for boundary work to
occur and initiate and terminate walking.
"""
function simulatesteps(w::Walk, msr::MultiStepResults; n=12, POCO=true, tchange=0)
    numsteps = length(msr.steps)
    n1step = n+POCO # simulatestep returns either n or n+1 pts, depending on POCO 
    padding = 3 # we add one boundary velocity and one initiation speed and one termination speed
    ts = [0., tchange]
    θs = [0., 0.]
    Ωs = [-msr.boundaryvels[1], -msr.vm0]
    KEs = [0., 1/2*msr.vm0^2]
    PEs = [0., 0.]
    vs = Array{Float64,2}(undef, numsteps*n1step+padding, 2)
    vs[1:2,:] = [msr.boundaryvels[1] 0; msr.vm0 0] # 2 x 2 matrix initiation
    nextheight = 0

    for i in eachindex(msr.steps)
        stepresult = msr.steps[i]
        t, θ, Ω, PE, KE, v, nextheight = simulatestep(w, stepresult, n=n, POCO=POCO, height=nextheight)
        append!(ts, t .+ ts[end])
        append!(θs, θ)
        append!(Ωs, Ω)
        append!(KEs, KE)
        append!(PEs, PE)
        vs[(i-1)*n1step+3:i*n1step+2,:] .= v
    end

    push!(ts, ts[end]+tchange)
    push!(θs, θs[end])
    push!(Ωs, -msr.boundaryvels[2])
    push!(KEs, 1/2*msr.boundaryvels[2]^2)
    push!(PEs, nextheight)
    vs[end,:] = [msr.boundaryvels[2] 0] # 1 x 2 matrix termination
    
    return (t=ts,θ = θs, Ω=Ωs,KE=KEs,PE=PEs,v=vs)
end