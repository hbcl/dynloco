# sensitivity analysis
# this works with package extension `DynLocoMakieExt.jl``, where if CairoMakie is imported,
# the associated plotting functions `plot3dsensitivity` and `plot2dsensitivity` are
# defined. 

export multistepsensitivity, plot2dsensitivity, plot3dsensitivity, plot2dsensitivity!, plot3dsensitivity!, boundingrect
# DynLocoMakieExt is a package extension with some sensitivity analyses
function plot2dsensitivity(); end
function plot3dsensitivity(); end
function boundingrect(); end # used in DynLocoMakieExt
function plot2dsensitivity!(); end
function plot3dsensitivity!(); end

"""
  multistepsensitivity(w::Walk, optresults::MultiStepResults; Pmultiplier=0.9:0.02:1.1) 
  
  Perform sensitivity analysis of an optimized multi-step sequence with respect to push-offs
  and gait initiation speed vm0. Optional `Pmultiplier` specifies the deviations as multipliers.
  Optional `centering = true` offsets both `workcosts` and `timecosts` to coincide at optimum at 0. `ct = optresults.metadata.ct` scales time into energy units.

  Returns a named tuple that can be deconstructed with 
  `(; steps, Psplusvars, workcosts, timecosts) = multistepsensitivity...`
   where all are 2D arrays (# of steps + 1, # of variations). 
   `steps` are the step numbers, `Psplusvars` are the vm0 and push-offs as varied, `workcosts`
   and `timecosts` are the associated costs.
"""
function multistepsensitivity(w::Walk, optresults::MultiStepResults; Pmultiplier::AbstractVector = 0.9:0.02:1.1, 
    centering = true, ct = optresults.metadata.ct)
    N = length(optresults.steps)
    Pstar = optresults.steps.P # a vector of optimal P
    workcosts = Array{Float64,2}(undef, length(Pmultiplier), N+1) # each column variations, row is step
    timecosts = similar(workcosts)
    Psplusvars = similar(workcosts)
    steps = similar(workcosts, Int)
    for j in 1:N+1 # prepend N steps with gait initiation speed vm0, for N+1 variations
        for i in eachindex(Pmultiplier)
            Psplus = [optresults.vm0; optresults.steps.P] # prepend vm0 to along with Ps
            Psplus[j] = Psplus[j]*Pmultiplier[i]
            initiationwork = 1/2*(Psplus[1]^2 - optresults.boundaryvels[1]^2) # Psplus[1] == vm0
            workcosts[i,j], timecosts[i,j] = multistep(w, Ps=Psplus[2:end], boundaryvels=optresults.boundaryvels, vm0=Psplus[1], tchange = 0., extracost=initiationwork) |> x->(x.totalcost, x.totaltime)
            Psplusvars[i,j] = Psplus[j] # store the variations in Psplus
            steps[i,j] = j - 1 # number steps from 0 to N
        end
    end
    timecosts .= ct * timecosts # cost of time converted into "energy"
    if centering
        whichbest = findfirst(==(1), Pmultiplier) # assuming Pmultiplier contains exactly 1
        workcosts .= workcosts .- workcosts[whichbest,1]
        timecosts .= timecosts .- timecosts[whichbest,1]
    end
    return (steps = steps, Psplusvars = Psplusvars, workcosts, timecosts, Pmultiplier = Pmultiplier)
end

# no boundary version
export multistepsensitivitynb
function multistepsensitivitynb(w::Walk, optresults::MultiStepResults; Pmultiplier::AbstractVector = 0.9:0.02:1.1, 
    centering = true, ct = optresults.metadata.ct)
    N = length(optresults.steps)
    Pstar = optresults.steps.P # a vector of optimal P
    workcosts = Array{Float64,2}(undef, length(Pmultiplier), N) # each column variations, row is step
    timecosts = similar(workcosts)
    Psplusvars = similar(workcosts)
    steps = similar(workcosts, Int)
    for j in 1:N 
        for i in eachindex(Pmultiplier)
            Psplus = copy(optresults.steps.P)
            Psplus[j] = Psplus[j]*Pmultiplier[i]
            workcosts[i,j], timecosts[i,j] = multistep(w, Ps=Psplus, boundaryvels=optresults.boundaryvels, tchange = 0.) |> x->(x.totalcost, x.totaltime)
            Psplusvars[i,j] = Psplus[j] # store the variations in Psplus
            steps[i,j] = j # number steps from 1 to N
        end
    end
    timecosts .= ct * timecosts # cost of time converted into "energy"
    if centering
        whichbest = findfirst(==(1), Pmultiplier) # assuming Pmultiplier contains exactly 1
        workcosts .= workcosts .- workcosts[whichbest,1]
        timecosts .= timecosts .- timecosts[whichbest,1]
    end
    return (steps = steps, Psplusvars = Psplusvars, workcosts, timecosts, Pmultiplier = Pmultiplier)
end