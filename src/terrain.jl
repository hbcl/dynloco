# functions for uneven terrain, such as plotting

export plotterrain, plotterrain!, stepoffirstbump


"""
    plotterrain(heights)

Plots a vector of discrete terrain `heights`, one per step, against step number.
Returns a `Plots.plot` struct that can be added to or displayed.
Each step is flat and centered at the step number.
Options include `bar_width = 1` (default), `fillto = -0.1` the bottom edge of
terrain, `steplength = 1` meaning integer steps. 

Use `plotterrain!(p, heights)` to add to an existing `p` plot.

See also `plotterrain(w::Walk, δs)` for continuous terrain plotting. 
"""
function plotterrain(heights::AbstractVector; plotoptions...)
    p = plot()
    plotterrain!(p, heights; plotoptions...)
end    

"""
    plotterrain(p, heights)
Adds a terrain plot to an existing plot `p`. See `plotterrain`.
"""
function plotterrain!(p::Union{Plots.Plot,Plots.Subplot}, heights::AbstractVector; setfirstbumpstep=false, steplength=1, fillto=-0.1, plotoptions... )
    stepnums = (1:length(heights)) .- stepoffirstbump(heights,setfirstbumpstep)
    bar!(p, steplength*stepnums, heights; bar_width=1, fillto=fillto, linewidth=0, plotoptions...)
end

"""
    `plotterrain(w::Walk, δs; terrainprofile=:continuous)` plots a continuous terrain profile to scale
    `plotterrain(w::Walk, δs; terrainprofile=:discrete)` plots a discrete terrain profile to scale

`w` is a Walk object, and `δs` is a vector of terrain slope changes, one for each step. Each footfall
is separated by one step length. The `terrainprofile` may be `:continuous` with piecewise linear
segments between each height, or `:discrete` with a flat foothold at each height like stairsteps.
The slope of each step is determined from the cumulative sum of preceding slope changes. An optional `offset` is added to vertically shift the terrain profile.
Step length is determined from `w`, but can be specified with optional keyword argument `steplength`.
The step numbers may be offset with `setfirstbumpstep` to align the plotted zeroth step with a desired step number.   
For `:discrete` terrain, the slopes are converted into appropriate step heights. An alternative is to
call `plotterrain(heights)` with a vector of heights, one per step.

`plotterrain` returns a plot object `p`. The default `aspect_ratio` is 1.
`plotterrain!` adds to an existing plot object `p`.
"""
function plotterrain(w::Walk, δs::AbstractVector; plotoptions...)
    p = plot()
    plotterrain!(p, w, δs; plotoptions...)
    return p
end


function plotterrain!(p::Union{Plots.Plot,Plots.Subplot}, w::Walk, δs::AbstractVector, aspect_ratio = 1; steplength = onestep(w).steplength, 
    setfirstbumpstep=false, offset=0., terrainprofile = :continuous, plotoptions...)
    if terrainprofile == :continuous
        # slope angle changes δs start relative to zero slope, so we pad a zero to the beginning
        # of the steps and their heights. 
        stepnums = (0:length(δs)) .- stepoffirstbump([0;δs],setfirstbumpstep)
        plot!(p, steplength .* stepnums, steplength .* cumsum(tan.([0;δs])) .+ offset; aspect_ratio=aspect_ratio, plotoptions...)
    else # :discrete, so use the flat-top stair-step version
        heights = steplength .* tan.(cumsum([0;δs])) .+ offset
        plotterrain!(p, heights; aspect_ratio=aspect_ratio, steplength=steplength, setfirstbumpstep=setfirstbumpstep, 
            plotoptions...)
    end
    return p
end



# Helper functions, unexported
"""
    stepoffirstbump(δs, setfirstbumpstep = false)

Determines which step the first bump occurs on for terrain `δs`. Returns an
integer referring to which step/index of `δs` should be treated as time 0.
`setfirstbumpstep` = `true``   determines first step that has non-zero `δ`
                   = false     ignores terrain, returns step 0
                   = N         returns N as set by user
Used for plotting speeds and terrains, to align t=0 to a common step.
"""
function stepoffirstbump(δs, setfirstbumpstep::Bool = false)
    if setfirstbumpstep && any(!iszero(δs))
        firstbump = findfirst(!iszero, δs)
    else
        firstbump = 0 # no shift in step number
    end
end

stepoffirstbump(δs, setfirstbumpstep::Real) = setfirstbumpstep
